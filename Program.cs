﻿using System;

namespace week3_ex16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine("***************************");
            Console.WriteLine("**** Welcome to my app ****");
            Console.WriteLine("***************************");
            Console.WriteLine("");
            Console.WriteLine("***************************");
            Console.WriteLine("     What is your name?");
            var userName = (Console.ReadLine());
            Console.WriteLine($"    Your name is {userName}");
        }
    }
}
